package com.ract.ins.proxy;

import com.ract.common.pool.ProgressProxyObjectFactory;
import com.ract.util.LogUtil;
import com.ract.ins.*;

public class InsProxyFactory extends ProgressProxyObjectFactory 
{
	  public final static String KEY_RACT_INS_PROXY = "RACT_INS_PROXY";
	  
	  public Object makeObject(Object key) throws java.lang.Exception
	  {
	    if (key == null)
	    {
	      throw new Exception("Attempting to make an object with a null key.");
	    }

	    if (key.toString().equals(KEY_RACT_INS_PROXY))
	    {
	      return new com.ract.ins.proxy.InsProxy();
	    }
	    else
	    {
	    	//can make any other type
	    	return super.makeObject(key);
	    }
	  }

}
