package com.ract.ins.proxy;

import com.progress.open4gl.ConnectException;
import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.SystemErrorException;
import com.ract.common.CommonConstants;
import com.ract.ins.RactInsProxy;

public class InsProxy extends RactInsProxy  implements com.ract.common.proxy.ProgressProxy{

	public InsProxy()throws Exception
	{
		   
		super(CommonConstants.getProgressAppServerURL(),  
				 CommonConstants.getProgressAppserverUser(),
			     CommonConstants.getProgressAppserverPassword(),
			     null);
	}


}
