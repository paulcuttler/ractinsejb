package com.ract.ins.proxy;
import org.apache.commons.pool.*;
import org.apache.commons.pool.impl.*;
import com.ract.util.*;

public class InsProxyPool 
{
	   public InsProxyPool(){}
	   private static GenericKeyedObjectPool pool;

	   public synchronized static GenericKeyedObjectPool getInstance()
	   {
	     if (pool == null)
	     {
	       pool = new GenericKeyedObjectPool(new InsProxyFactory(),
	                                         20,
	                                         GenericKeyedObjectPool.WHEN_EXHAUSTED_GROW,
	                                         GenericKeyedObjectPool.DEFAULT_MAX_WAIT);
	       //test the object on return to the pool to see if it is still valid
	       pool.setTestOnReturn(true);
	       pool.setTestOnBorrow(true);
	     }
	     return pool;
	   }

	   public static void removeCurrentObjectPool()
	   {
	     if (pool != null)
	     {
	       pool.clear();
	       try
	       {
	         pool.close();
	       }
	       catch(Exception ex)
	       {
	         LogUtil.log("InsProxyPool","Unable to close the object pool. "+ex.getMessage());
	       }
	       pool = null;
	     }
	   }

}
