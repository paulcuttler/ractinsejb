package com.ract.ins.claim;

import java.rmi.RemoteException;

import javax.ejb.Stateless;

import com.ract.ins.proxy.InsProxy;

@Stateless
public class InsClaimMgr implements InsClaimMgrRemote 
{
	 ProgressAdapter adp = null;
	
	 public Integer newClaimBatch()throws Exception
	 {
		 return this.getAdapter().allocateBatchNo();
	 }
	 public String sendTask(TaskRow tr)throws Exception
	 {
		return this.getAdapter().processLine(tr);
	 }
	 
	 private ProgressAdapter getAdapter()
	 {
		 if(this.adp == null) this.adp = new ProgressAdapter();
		 return this.adp;
	 }
	 
	 public String validateHeadings(String loadType,
			                        String headingList)throws Exception
	 {
		 return this.getAdapter().validateHeadings(loadType, headingList);
	 }

}
