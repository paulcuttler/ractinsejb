package com.ract.ins.claim;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;

import com.progress.open4gl.IntHolder;
import com.progress.open4gl.StringHolder;
import com.ract.common.SystemException;
import com.ract.ins.proxy.*;
import com.ract.ins.*;
import com.ract.progress.InputResultSetM;
import com.ract.util.LogUtil;

public class ProgressAdapter 
{
	
    InsProxy pxy = null;	
    public Integer allocateBatchNo()throws Exception
    {
    	Integer batchNo = null;
    	InsProxy px = null;
    	try
    	{
    		px = this.getProxy();
    		IntHolder ih = new IntHolder();
    		px.allocateBatchNumber(ih);
    		batchNo = new Integer(ih.getIntValue());
    	}
    	catch(Exception ex)
    	{
    		LogUtil.log(this.getClass(), "Error getting batchNo " + ex.getMessage());
    		ex.printStackTrace();
    		throw new Exception("Error getting batch number from Progress system");
    	}
    	finally 
    	{
    		this.releaseProxy(px);
    	}
    	return batchNo;
    }
    
    public String processLine(TaskRow tr)throws Exception
    {
    	InsProxy px = null;
    	String os = null;
    	try
    	{
    		px = this.getProxy();
    		LineHolder lh = new LineHolder(tr);
    		StringHolder sh = new StringHolder();
    		px.processLine(tr.getBatchNo().intValue(),
    				       tr.getLineNo().intValue(),
    				       tr.getProcessType(),
    				       false,
    				       lh,
    				       sh);
    		os = sh.getStringValue();
    	}
    	catch(Exception ex)
    	{
    		LogUtil.log(this.getClass(), "Error getting batchNo " + ex.getMessage());
    		ex.printStackTrace();
    		throw new Exception("Error sending data for ProcessLine");
    	}
    	finally
    	{
    		this.releaseProxy(px);
    	}
    	return os;
    }
    
    private InsProxy getProxy()throws SystemException
    {
    	if(this.pxy==null)
        {
    		try
    		{
    			this.pxy = (InsProxy)InsProxyPool.getInstance().borrowObject(InsProxyFactory.KEY_RACT_INS_PROXY);
    		}
    		catch(Exception ex)
    		{
    			throw new SystemException(ex);
    		}
        }
    	return pxy;
    }
    private void releaseProxy(InsProxy proxy)
    {
    	try 
    	{
			InsProxyPool.getInstance().returnObject(InsProxyFactory.KEY_RACT_INS_PROXY, proxy);
		} 
    	catch (Exception ex)
    	{
			LogUtil.fatal(this.getClass(), "Error releasing insurance progress proxy. Exception:" + ex.getMessage());
		}
    	finally
    	{
    		this.pxy = null;
    	}
    }
    
    private class LineHolder extends InputResultSetM
    {
        TaskRow row = null;
        Enumeration en = null;
        Object key = null;
        Hashtable data = null;
    	
    	public LineHolder(TaskRow tr)
    	{
    		this.row = tr;
    		this.data = tr.getColumns();
    		en = this.data.keys();
   	}
		@Override
		public Object getObject(int columnNo) throws SQLException {
			if(columnNo == 1)return this.key;
			else if(columnNo ==2) return data.get(this.key);
			else
			{
				return null;

			}
		}

		@Override
		public boolean next() throws SQLException {
			if(en.hasMoreElements()) 
			{
				key = en.nextElement();
				return true;
			}
			else return false;
		}

    }
    
    public String validateHeadings(String loadType,String headingList)throws Exception
    {
    	InsProxy px = null;
    	String errorList = null;
    	try
    	{
    		px = this.getProxy();
    		StringHolder errorListHolder = new StringHolder();
    		px.validateHeadings(loadType,
    				            headingList,
    				            errorListHolder);
     		errorList = errorListHolder.getStringValue();
    	}
    	catch(Exception ex)
    	{
    		LogUtil.log(this.getClass(), "Error getting batchNo " + ex.getMessage());
    		ex.printStackTrace();
    		throw new Exception("Error sending data for validateHeadings");
    	}
    	finally
    	{
    		this.releaseProxy(px);
    	}
    	return errorList;

    }
    
    
}
