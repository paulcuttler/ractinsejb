package com.ract.ins.claim;

import java.io.Serializable;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;

public class TaskRow implements Serializable
{
	private Hashtable<String,String> columns;
	private Integer batchNo;
	private Integer lineNo;
	private String processType;
	
	public TaskRow()
	{
		columns = new Hashtable<String,String>();
	}
	public Integer getBatchNo() {
		return batchNo;
	}
	public void setBatchNo(Integer batchNo) {
		this.batchNo = batchNo;
	}
	public Integer getLineNo() {
		return lineNo;
	}
	public void setLineNo(Integer lineNo) {
		this.lineNo = lineNo;
	}
	public String getProcessType() {
		return processType;
	}
	public void setProcessType(String processType) {
		this.processType = processType;
	}

	public void putValue(String fieldName,String fieldValue)
	{
		if(fieldName!=null)	columns.put(fieldName,fieldValue);
	}
	public String getValue(String fieldName)
	{
		if(columns.containsKey(fieldName))
		{
			return (String)columns.get(fieldName);
		}
		else return null;
	}
	public Hashtable getColumns() {
		return columns;
	}

	public String toString()
	{
		String os = "BatchNo: " + this.batchNo
		+ "   LineNo:  " + this.lineNo
		+ "   Process: " + this.processType;
		Iterator en = columns.keySet().iterator();
		while(en.hasNext())
		{
			Object el = en.next();
			os += "\nColumn Name = " + el + ":  \t value = " + columns.get(el);
		}	
		return os;
	}
}
