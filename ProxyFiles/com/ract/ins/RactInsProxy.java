/*
**
**    Created by PROGRESS ProxyGen (Progress Version 10.1C) Tue May 10 14:13:40 EST 2011
**
*/

package com.ract.ins;

import com.progress.open4gl.*;
import com.progress.common.ehnlog.IAppLogger;
import com.progress.common.ehnlog.LogUtils;
import com.progress.open4gl.dynamicapi.IPoolProps;
import com.progress.open4gl.javaproxy.Connection;
import com.progress.message.jcMsg;
import java.math.BigDecimal;
import java.util.Date;
import java.sql.ResultSet;
import java.io.IOException;

//
// RactInsProxy
//
/**
*    
*
*    @author    dgk
*    @version 
*/
public class RactInsProxy implements SDOFactory
{
    // "This proxy version is not compatible with the current
    // version of the dynamic API."
    protected static final long m_wrongProxyVer = jcMsg.jcMSG079;

    private   static final int  PROXY_VER = 5;

    protected RactInsProxyImpl m_RactInsProxyImpl;

    //---- Constructors
    public RactInsProxy(Connection connection)
        throws Open4GLException,
               ConnectException,
               SystemErrorException,
               IOException
    {
        /* we must do this here before we attempt to create the appobject */
        if (RunTimeProperties.getDynamicApiVersion() != PROXY_VER)
            throw new Open4GLException(m_wrongProxyVer, null);

        String urlString = connection.getUrl();
        if (urlString == null || urlString.compareTo("") == 0)
            connection.setUrl("com.ract.ins.RactInsProxy");

        m_RactInsProxyImpl = new RactInsProxyImpl(
                                  "com.ract.ins.RactInsProxy",
                                  connection,
                                  RunTimeProperties.tracer);
    }

    public RactInsProxy(String urlString,
                        String userId,
                        String password,
                        String appServerInfo)
        throws Open4GLException,
               ConnectException,
               SystemErrorException,
               IOException
    {
        Connection connection;

        /* we must do this here before we attempt to create the appobject */
        if (RunTimeProperties.getDynamicApiVersion() != PROXY_VER)
            throw new Open4GLException(m_wrongProxyVer, null);

        connection = new Connection(urlString,
                                    userId,
                                    password,
                                    appServerInfo);

        m_RactInsProxyImpl = new RactInsProxyImpl(
                                  "com.ract.ins.RactInsProxy",
                                  connection,
                                  RunTimeProperties.tracer);

        /* release the connection since the connection object */
        /* is being destroyed.  the user can't do this        */
        connection.releaseConnection();
    }

    public RactInsProxy(String userId,
                        String password,
                        String appServerInfo)
        throws Open4GLException,
               ConnectException,
               SystemErrorException,
               IOException
    {
        Connection connection;

        /* we must do this here before we attempt to create the appobject */
        if (RunTimeProperties.getDynamicApiVersion() != PROXY_VER)
            throw new Open4GLException(m_wrongProxyVer, null);

        connection = new Connection("com.ract.ins.RactInsProxy",
                                    userId,
                                    password,
                                    appServerInfo);

        m_RactInsProxyImpl = new RactInsProxyImpl(
                                  "com.ract.ins.RactInsProxy",
                                  connection,
                                  RunTimeProperties.tracer);

        /* release the connection since the connection object */
        /* is being destroyed.  the user can't do this        */
        connection.releaseConnection();
    }

    public RactInsProxy()
        throws Open4GLException,
               ConnectException,
               SystemErrorException,
               IOException
    {
        Connection connection;

        /* we must do this here before we attempt to create the appobject */
        if (RunTimeProperties.getDynamicApiVersion() != PROXY_VER)
            throw new Open4GLException(m_wrongProxyVer, null);

        connection = new Connection("com.ract.ins.RactInsProxy",
                                    null,
                                    null,
                                    null);

        m_RactInsProxyImpl = new RactInsProxyImpl(
                                  "com.ract.ins.RactInsProxy",
                                  connection,
                                  RunTimeProperties.tracer);

        /* release the connection since the connection object */
        /* is being destroyed.  the user can't do this        */
        connection.releaseConnection();
    }

    public void _release() throws Open4GLException, SystemErrorException
    {
        m_RactInsProxyImpl._release();
    }

    //---- Get Connection Id
    public Object _getConnectionId() throws Open4GLException
    {
        return (m_RactInsProxyImpl._getConnectionId());
    }

    //---- Get Request Id
    public Object _getRequestId() throws Open4GLException
    {
        return (m_RactInsProxyImpl._getRequestId());
    }

    //---- Get SSL Subject Name
    public Object _getSSLSubjectName() throws Open4GLException
    {
        return (m_RactInsProxyImpl._getSSLSubjectName());
    }

    //---- Is there an open output temp-table?
    public boolean _isStreaming() throws Open4GLException
    {
        return (m_RactInsProxyImpl._isStreaming());
    }

    //---- Stop any outstanding request from any object that shares this connection.
    public void _cancelAllRequests() throws Open4GLException
    {
        m_RactInsProxyImpl._cancelAllRequests();
    }

    //---- Return the last Return-Value from a Progress procedure
    public String _getProcReturnString() throws Open4GLException
    {
        return (m_RactInsProxyImpl._getProcReturnString());
    }

    //---- Create an SDO ResultSet object - There are 3 overloaded variations
    public SDOResultSet _createSDOResultSet(String procName)
        throws Open4GLException, ProSQLException
    {
        return (m_RactInsProxyImpl._createSDOResultSet(procName, null, null, null));
    }

    public SDOResultSet _createSDOResultSet(String procName,
                                            String whereClause,String sortBy)
        throws Open4GLException, ProSQLException
    {
        return (m_RactInsProxyImpl._createSDOResultSet(procName, whereClause, sortBy, null));
    }

    public SDOResultSet _createSDOResultSet(String procName,
                                          String whereClause,
                                          String sortBy,
                                          SDOParameters params)
        throws Open4GLException, ProSQLException
    {
        return (m_RactInsProxyImpl._createSDOResultSet(procName, whereClause, sortBy, params));
    }

    // Create the ProcObject that knows how to talk to SDO's.
    public SDOInterface _createSDOProcObject(String procName)
        throws Open4GLException
    {
        return (m_RactInsProxyImpl._createSDOProcObject(procName));
    }

	/**
	*	
	*	
	*/
	public void allocateBatchNumber(IntHolder pBatchNo)
		throws Open4GLException, RunTime4GLException, SystemErrorException
	{
		m_RactInsProxyImpl.allocateBatchNumber( pBatchNo);
	}

	/**
	*	
	*	
	*	Schema of input result set; Parameter 5
	*		Field:fName character (java.lang.String)
	*		Field:fData character (java.lang.String)
	*/
	public void processLine(int pBatchNo, int pLineNo, String pProcess, boolean reSubmit, java.sql.ResultSet tData, StringHolder pResult)
		throws Open4GLException, RunTime4GLException, SystemErrorException
	{
		m_RactInsProxyImpl.processLine( pBatchNo,  pLineNo,  pProcess,  reSubmit,  tData,  pResult);
	}

	/**
	*	
	*	
	*/
	public void validateHeadings(String pLoadType, String pHeadingList, StringHolder pFailList)
		throws Open4GLException, RunTime4GLException, SystemErrorException
	{
		m_RactInsProxyImpl.validateHeadings( pLoadType,  pHeadingList,  pFailList);
	}



}
